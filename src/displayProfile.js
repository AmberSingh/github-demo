import React from 'react'

const image = require('./Resources/supreet.jpeg');
const target = require('./Resources/users.svg');
const location = require('./Resources/placeholder.svg');
const email = require('./Resources/mail.svg');
const stars = require('./Resources/star.svg');

export default function displayProfile(props) {
  return (
    <React.Fragment>
      <div className="container">
        <div className="left_Portion">
          <img className="suprjeet" src={image} alt='suprjeet'/>
          <div className="name">
            <h3>Supreet Singh</h3>
            <p>supreetsingh247</p>
          </div>
          <div className="line"/>
          <div className="follow">Follow</div>
          <p className="info">Front end developer since 1.5 years</p>

          <div className="target">
            <img src={target} alt="target" />
            <p>Target Corporation</p>
          </div>

          <div className="location">
            <img src={location} alt="location" />
            <p>India</p>
          </div>

          <div className="email">
            <img src={email} alt="email" />
            <p>suprejeetsingh@gmail.com</p>
          </div>

          <p className="block">Block or report user</p>

        </div>
        <div className="right_Portion">
          <div className="header">
            <p>Overview</p>
            <p>Repositories <strong>12</strong></p>
            <p>Projects <strong>0</strong></p>
            <p>Stars <strong>7</strong></p>
            <p>Followers <strong>4</strong></p>
            <p>Following <strong>2</strong></p>
          </div>
          <div className="line"/>
          <div className="popular">Popular repositories</div>

          <div className='row'>
            <div className='box1'>
              <p className="heading">{props[1].name}</p>
              <p className="text">{props[1].description}</p>
              <div className="footer">
                  <div className="circle"/>
                  <p>Javascript</p>
                <div>
                  <img src={stars} alt="stars"/>
                </div>
              </div>
            </div>

            <div className='box2'>
              <p className="heading">{props[0].name}</p>
              <p className="text">{props[0].description}</p>
            </div>
          </div>

          <div className='row'>
            <div className='box1'>
              <p className="heading">{props[5].name}</p>
              <p className="text">{props[5].description}</p>
              <div className="footer">
                  <div className="circle_purple"/>
                  <p>CSS</p>
              </div>
            </div>

            <div className='box2'>
              <p className="heading">{props[11].name}</p>
              <p className="text">{props[11].description}</p>
              <div className="footer">
                <div className="circle_purple"/>
                <p>CSS</p>
              </div>
            </div>
         </div>


         <div className='row'>
            <div className='box1'>
              <p className="heading">{props[8].name}</p>
              <p className="text">{props[8].description}</p>
              <div className="footer">
                  <div className="circle"/>
                  <p>JAVASCRIPT</p>
              </div>
            </div>

            <div className='box2'>
              <p className="heading">{props[6].name}</p>
              <p className="text">{props[6].description}</p>
              <div className="footer">
                <div className="circle"/>
                <p>CSS</p>
              </div>
            </div>
         </div>
         <div className="contributions">
          2 contributions in the last years
        </div>
        </div>
      </div>
    </React.Fragment>
  )
}
