/* 
  src/reducers/simpleReducer.js
*/
export default (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_API':
      return {
        result: action.payload
      }
    default:
      return state
  }
}
