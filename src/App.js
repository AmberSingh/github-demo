import React, { Component } from 'react';
import { connect } from 'react-redux';
import './displayProfile.css';
import displayProfile from './displayProfile.js';

import { getApiResults } from './actions/simpleAction'

/* 
 * mapDispatchToProps
*/
const mapDispatchToProps = dispatch => ({
  simpleAction: () => dispatch(getApiResults())
})

/* 
 * mapStateToProps
*/
const mapStateToProps = state => ({
  ...state
})

/**
 * @class App
 * @extends {Component}
 */
class App extends Component {
  /**
   * @memberof App
   * @summary handles button click 
   */

  state = {
    data: [],
  }
  simpleAction = (event) => {
    this.props.simpleAction();
  }
  componentDidMount(){
    this.props.simpleAction();
  }

  componentWillReceiveProps(nextProps){
    this.setState({data: nextProps.simpleReducer.result});
  }
  render() {
    
    return (


      <div className="App">
        {this.state.data.length>0 && displayProfile(this.state.data)}
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
