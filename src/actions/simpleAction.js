/* 
  src/actions/simpleAction.js
*/
export const fetchApi = (data) => dispatch => {
  dispatch({type: 'FETCH_API',payload: data});
}

export const getApiResults = () => {
  return (dispatch) => {
      fetch('https://api.github.com/users/supreetsingh247/repos', {
          method: "GET",
          headers: {}
        })
      .then(r=> r.json())
      .then(resp => {
          return  dispatch(fetchApi(resp)); 
      })
      .catch((error) => console.error(error) )
  }
}
